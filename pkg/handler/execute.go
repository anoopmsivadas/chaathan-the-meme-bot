package handler

import (
	"strings"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/anoopmsivadas/chaathan/config"
	"gitlab.com/anoopmsivadas/chaathan/pkg/context"
	"gitlab.com/anoopmsivadas/chaathan/pkg/meme"
)

func CmdHandler(s *discordgo.Session, m *discordgo.MessageCreate) {
	prefix := config.CmdPrefix
	if m.Author.ID == s.State.User.ID {
		return
	}
	if strings.HasPrefix(m.Content, prefix) {
		context.MakeHelpMessage()
		if m.Content == prefix+"help" {
			//s.ChannelMessageSend(m.ChannelID, "Hi <@"+m.Author.ID+">")
			s.ChannelMessageSendEmbed(m.ChannelID, &context.HelpMessage)
		}
		for _, command := range context.Commands {
			if m.Content == prefix+command.CMD {
				s.ChannelMessageSendEmbed(m.ChannelID, meme.MemeReddit(&command))
			}
		}
	}
}
