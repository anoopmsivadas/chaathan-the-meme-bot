package config

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
)

type config struct {
	Token     string `json:"token"`
	CmdPrefix string `json:"cmdPrefix"`
}

var (
	Token     string
	CmdPrefix string
	botConfig *config
)

func LoadConfig() error {
	file, err := ioutil.ReadFile("./config.json")
	if err != nil {
		fmt.Println("Error Reading Config File", err)
		return err
	}
	err = json.Unmarshal(file, &botConfig)
	if err != nil {
		fmt.Println("Error Loading the Config")
		return err
	}

	Token = botConfig.Token
	CmdPrefix = botConfig.CmdPrefix
	return nil
}
