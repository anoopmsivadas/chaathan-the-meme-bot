# Chaathan The Discord Meme Bot

<img src="web/assets/chaathan.png" width="200px">

A Friendly Neighborhood Kuttichaathan that Provides Memes.
It's just an experiment with Go and Discord API using [DiscordGo](https://github.com/bwmarrin/discordgo).
 As for now the bot can scrape memes from Reddit only.

  *Chaathan(ചാത്തന്‍) is a mythical creature that can be found in Malayalam Comics.
 
 ---

## How to Run

You need Go and Python 3.x installed on the machine to run this. And both [Discord](https://discord.com/developers/applications) and [Reddit](https://old.reddit.com/wiki/api) API credentials. The Discord API Configuration is stored in [config.json](https://gitlab.com/anoopmsivadas/chaathan-the-meme-bot/-/blob/master/config.json).<br>
Reddit API config has to be set on the script [utils/scraper/reddit.py](https://gitlab.com/anoopmsivadas/chaathan-the-meme-bot/-/blob/master/utils/scraper/reddit.py)   itself (sorry for that!).

 In Local Machine create a virtual environment for Python(optional) 

 Run ``make init`` to install all the dependencies

 Run ``make run`` to Run the Bot.

 All required configurations for Heroku is included in the repo.

 Setup Heroku CLI

Run the following from the Project Folder ``heroku create``

 Run ``heroku stack:set container``

 Run ``git push heroku master``

## License

Confused between WTFPL and MIT, let's go with [MIT](https://gitlab.com/anoopmsivadas/chaathan-the-meme-bot/-/blob/master/LICENSE) since I have to maintain professionalism
